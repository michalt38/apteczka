<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
		header("Location: index.php?wybrano=0");

	require_once "inc/nagl.php";
	require_once "inc/menu.php";
	require_once "inc/funkcje.php";
	
	require_once 'conf/zmienne.php';
	require_once 'inc/baza.php';
	
	?>
	<div class="container">
	<ul class="nav nav-pills">
	<?php
	$apteczka_num = 1;
	$query = "SELECT * FROM apteczki WHERE `idkonta` ='" . $_SESSION['zalogowany'] . "'";
	$wynik = $baza->query($query);
	$apteczka = "";
	if(isset($_GET["apteczka"]))
		$apteczka = $_GET["apteczka"];
		while($row = $wynik->fetch_assoc())
		{
			if(!isset($_GET["apteczka"]) && $apteczka_num == 1)
			{
				$apteczka = $row["nazwa"];
			}
			?>
	    	<li <?php if((!isset($_GET["apteczka"]) && $apteczka_num == 1) || $_GET["apteczka"] ==  $row["nazwa"]) echo 'class="active"'; ?>><a href="historia.php?wybrano=2&apteczka=<?php echo $row["nazwa"];?>">Apteczka <?php echo $apteczka_num++;?></a></li>
	<?php 
		}
		
		$query = "SELECT * FROM dostep WHERE `idkonta` ='" . $_SESSION['zalogowany'] . "'";
		$wynik = $baza->query($query);
		while($row = $wynik->fetch_assoc())
		{
			$query = "SELECT * FROM apteczki, konta WHERE apteczki.idkonta = '" . $row["dostep_do_konta"] . "' AND apteczki.idkonta = konta.idkonta";
			$wynik2 = $baza->query($query);
			while($row2 = $wynik2->fetch_assoc())
			{	
				
				$label = $row2["imie"][0] . $row2["nazwisko"][0];
	?>
				<li <?php if($_GET["apteczka"] ==  $row2["nazwa"]) echo 'class="active"';?>><a href="historia.php?wybrano=2&apteczka=<?php echo $row2["nazwa"];?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $row2["imie"] . " " . $row2["nazwisko"] ?>">Apteczka <?php echo $apteczka_num++;?> 
				<span class="label label-default"><?php echo $label ?></span> </a></li>
	<?php
			}
		}
	?>    
	  	</ul>
	  	<br>
<?php
	$query = "select * from ".$apteczka.", konta, leki_specyfikacja where ".$apteczka.".idkonta=konta.idkonta AND ".$apteczka.".idlek=leki_specyfikacja.idleki";
	
	$wynik = $baza->query($query);
?>

	<table class="table table-striped">
		<thead>
      		<tr>
      			<th>Nr dok.</th>
        		<th>Nazwa leku</th>
        		<th>EAN</th>
        		<th>Op. zbiorcze</th>
        		<th>Ilość</th>
        		<th>Cena</th>
        		<th>Termin ważności</th>        		
        		<th>Kto</th>
        		<th>Kiedy</th>
        		<th>Typ dok.</th>
        		<th>Dok. źr.</th>
      		</tr>
    	</thead>
    	<tbody>
<?php

	while($row = $wynik->fetch_assoc()) {
		?><tr>
		<td><?php echo $row["id"]; ?></td>
		<td><?php echo $row["nazwa"]; ?></td>
		<td><?php echo $row["ean"]; ?></td>
		<td><?php echo $row["op_zb"]; ?></td>
		<td><?php echo $row["ile"]; ?></td>
		<td><?php if(!empty($row["cena"]))echo $row["cena"]." zł"; ?></td>
		<td><?php echo $row["data_waz"]; ?></td>
		<td><?php echo $row["imie"]." ".$row["nazwisko"]; ?></td>
		<td><?php echo $row["data"];?></td>
		<td><?php echo $row["typ"];?></td>
		<td><?php echo $row["dok_zr"];?></td>
		</tr>
		<?php
	}

?></tbody></table>
</div>
<?php
	
	require_once "inc/stopka.php";
?>