<?php
	session_start();

	require_once "inc/nagl.php";
	require_once "inc/menu.php";
	require_once "inc/funkcje.php";
	
	require_once 'conf/zmienne.php';
	require_once 'inc/baza.php';
	
	$dobry_mail = true;
	
	if(isset($_POST["email"]))
	{
		$query = "select * from konta";
		$wynik = $baza->query($query);
		while($row = $wynik->fetch_assoc())
		{
			if($row["email"] == $_POST["email"])
				$dobry_mail = false;
		}
	}
	
	if(isset($_POST["email"]) && $dobry_mail == false)
	{
?>
		<div class="container">
			<div class="alert alert-danger">
			Podany mail już istnieje.
			</div>
		</div>
<?php
	}

	$dodaj_uzytkownika = false;
	if(isset($_POST["email"]) && $dobry_mail == true)
	{
		$dodaj_uzytkownika = true;
		$query = "select * from konta";
		$wynik = $baza->query($query);
		
		$iddostep = 1;
		while($row = $wynik->fetch_assoc())
		{
				
			if(!empty($_POST["user" . $iddostep]))
			{
				if($row["haslo"] != base64_encode($_POST["user" . $iddostep]))
				{
					$dodaj_uzytkownika = false;
				?>
					<div class="container">
						<div class="alert alert-danger">
						Złe hasło dostępu do konta: <?php echo $row["imie"] . " " . $row["nazwisko"]?>
						</div>
					</div>
				<?php
				}
			}
				
			$iddostep++;
		}
	}
	if($dodaj_uzytkownika)
	{
		$query = "START TRANSACTION";
		$baza->query($query);
		$haslo = base64_encode($_POST["haslo"]);
		$query = "insert into konta (`imie`, `nazwisko`, `email`, `haslo`) values ('" . $_POST["imie"] . "', '" . $_POST["nazwisko"] . "', '" . $_POST["email"] . "', '" . $haslo . "')";
		$baza->query($query);
		
		$query = "select max(idapteczki) from apteczki";
		$wynik = $baza->query($query);
		$idapteczki = $wynik->fetch_assoc();
		$idapteczki = $idapteczki["max(idapteczki)"];
		
		//echo $idapteczki;
		
		$query = "select max(idkonta) from konta";
		$wynik = $baza->query($query);
		$idkonta = $wynik->fetch_assoc();
		$idkonta = $idkonta["max(idkonta)"];
		
		$nowe_apteczki = $_POST["apteczki"];
		echo $nowe_apteczki . "<br>";
		
		while($nowe_apteczki)
		{
			$idapteczki++;
			$nazwa_apteczki = "apteczka" . $idapteczki;
			$query = "
			create table " . $nazwa_apteczki . " (
			id bigint(20) not null auto_increment,
			idkonta bigint(20) not null,
			idlek bigint(20) not null,
			data date not null,
			ile int(11) not null,
			ile_poz int(11) null,
			dok_zr bigint(20) null,
			data_waz date null,
			cena float null,
			typ varchar(40) not null collate utf8_polish_ci,
			primary key(id),
			foreign key(idkonta) references `milchalt`.`konta`(`idkonta`),
			foreign key(idlek) references `milchalt`.`leki_specyfikacja`(`idleki`),
			foreign key(dok_zr) references `milchalt`.`" . $nazwa_apteczki ."`(`id`)
			)";
			
			$baza->query($query);
			echo $query . "<br>";
			
			$query = "insert into apteczki (`idkonta`, `nazwa`) VALUES ('" . $idkonta . "', '" . $nazwa_apteczki . "')";
			$baza->query($query);
			
			$nowe_apteczki--;
		}
		
		$query = "select * from konta";
		$wynik = $baza->query($query);
		
		$iddostep = 1;
		while($row = $wynik->fetch_assoc())
		{
			
			if(isset($_POST["user" . $iddostep]) && $row["haslo"] == base64_encode($_POST["user" . $iddostep]))
			{
				$query = "insert into dostep (`idkonta`, `dostep_do_konta`) VALUES ('" . $idkonta . "', '" . $iddostep . "')";
				echo $query . "<br>";
				$baza->query($query);
			}
			
			$iddostep++;
		}
		$query = "COMMIT";
		$baza->query($query);
		header("Location: index.php?wybrano=0");
		
	}
	
?>
	<div class="container">
		<h2>Dodaj nowego użytkownika</h2><br>
		<form action="" method="POST" class="form-horizontal">
			<div class="form-group">
      			<label class="col-sm-2 control-label">Imię</label>
      			<div class="col-sm-4">
        			<input class="form-control" id="imie" name="imie" type="text" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Nazwisko</label>
      			<div class="col-sm-4">
        			<input class="form-control" id="nazwisko" name="nazwisko" type="text" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Adres e-mail</label>
      			<div class="col-sm-4">
        			<input class="form-control" id="email" name="email" type="email" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Hasło</label>
      			<div class="col-sm-4">
        			<input class="form-control" id="haslo" name="haslo" type="password" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Liczba apteczek</label>
      			<div class="col-sm-4">
        			<input class="form-control" id="apteczki" name="apteczki" type="number" min="1" max="10" required="true">
      			</div>
      		</div>
      		<br><h4>Dostęp do innych kont</h4><br>
<?php
      		$query = "SELECT * FROM konta";
			$wynik = $baza->query($query);
			$user = 1;
			while($row = $wynik->fetch_assoc())
			{
				$name = "user" . $user++;
				?>
				
				<div class="form-group">
      				<label class="col-sm-2 control-label"><?php echo $row["imie"] . " " . $row["nazwisko"]; ?></label>
      				<div class="col-sm-4">
        				<input class="form-control" id=<?php echo $name; ?> name=<?php echo $name; ?> type="password" placeholder="Hasło">
      				</div>
      			</div>
      			
				<?php
			}
			
?>
			<br>
      		<div class="col-sm-offset-3">
      			<input type="submit" class="btn btn-info" value="Zarejestruj się">
			</div>
		</form>
	</div>
<?php 
	require_once "inc/stopka.php";
?>