<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
		header("Location: index.php?wybrano=0");

	require_once "inc/nagl.php";
	require_once "inc/menu.php";
	require_once "inc/funkcje.php";
	
	require_once 'conf/zmienne.php';
	require_once 'inc/baza.php';

	date_default_timezone_set('Europe/Warsaw');
?>
	<div class="container">

	<ul class="nav nav-pills">
<?php 
	$apteczka_num = 1;
	$query = "SELECT * FROM apteczki WHERE `idkonta` ='" . $_SESSION['zalogowany'] . "'";
	$wynik = $baza->query($query);
	$apteczka = "";
	if(isset($_GET["apteczka"]))
		$apteczka = $_GET["apteczka"];
	while($row = $wynik->fetch_assoc())
	{
		if(!isset($_GET["apteczka"]) && $apteczka_num == 1)
		{
			$apteczka = $row["nazwa"];
		}
?>	
    	<li <?php if((!isset($_GET["apteczka"]) && $apteczka_num == 1) || $_GET["apteczka"] ==  $row["nazwa"]) echo 'class="active"'; ?>><a href="moje_leki.php?wybrano=1&apteczka=<?php echo $row["nazwa"];?>">Apteczka <?php echo $apteczka_num++;?></a></li>
<?php 
	}
	
	$query = "SELECT * FROM dostep WHERE `idkonta` ='" . $_SESSION['zalogowany'] . "'";
	$wynik = $baza->query($query);
	while($row = $wynik->fetch_assoc())
	{
		$query = "SELECT * FROM apteczki, konta WHERE apteczki.idkonta = '" . $row["dostep_do_konta"] . "' AND apteczki.idkonta = konta.idkonta";
		$wynik2 = $baza->query($query);
		while($row2 = $wynik2->fetch_assoc())
		{	
			
			$label = $row2["imie"][0] . $row2["nazwisko"][0];
?>
			<li <?php if($_GET["apteczka"] ==  $row2["nazwa"]) echo 'class="active"';?>><a href="moje_leki.php?wybrano=1&apteczka=<?php echo $row2["nazwa"];?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $row2["imie"] . " " . $row2["nazwisko"] ?>">Apteczka <?php echo $apteczka_num++;?> 
			<span class="label label-default"><?php echo $label ?></span> </a></li>
<?php
		}
	}
?>    
  	</ul>
<?php 
	$query = "select * from leki_specyfikacja";
	$wynik = $baza->query($query);
	$index=0;
	while($row = $wynik->fetch_assoc())
	{
		$leki[$index] = $row["nazwa"] . " | " . $row["subst_czynna"] . " | " . $row["ean"] . " | " . $row["op_zb"];
		$index++;
	}
	$max_leki = $index;
?>
  	<br>
		<div class="panel panel-default">
			<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" href="#collapse1">Dodaj leki</a>
			</h4>
			</div>
			<div id="collapse1" class="panel-collapse collapse">
				<div class="panel-body">
				<form action="" method="POST">
				
					<label for="nazwa">Nazwa</label>
					<select id="nazwa" class="form-control" name="nazwa">
<?php 
					$query = "select * from leki_specyfikacja";
					$wynik = $baza->query($query);
					for($index=0;$index<$max_leki;$index++)
					{
						$row = $wynik->fetch_assoc();
						echo '<option value='. $row["idleki"] . '>' . $leki[$index] . "</option>";
					}
?>
  					</select>
				
				<br>
				<div class="row">
						<div class="col-xs-4">
							<label for="ilosc">Ilość</label>
							<input type="number" class="form-control" id="ilosc" name="ilosc" min="1">
						</div>
						
						<div class="col-xs-4">
							<label for="cena">Cena</label>
							<input type="number" step="0.01" class="form-control" id="cena" name="cena" min="0.01">
						</div>
						
						<div class="col-xs-4">
							<label for="termin">Data ważności</label>
							<input type="date" class="form-control" id="termin" name="termin">
						</div>		
					
				</div>
				<br>
				<input type="submit" class="btn btn-info pull-right" value="Wykonaj">
				</form>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" href="#collapse2">Pobierz leki</a>
			</h4>
			</div>
			<div id="collapse2" class="panel-collapse collapse">
			<div class="panel-body">
				<form action="" method="POST">
				<div class="row">
				<div class="col-xs-7">
					<label for="nazwa_pobranie">Nazwa</label>
					<select id="nazwa_pobranie" class="form-control" name="nazwa_pobranie">
<?php 
					$query = "select * from leki_specyfikacja";
					$wynik = $baza->query($query);
					for($index=0;$index<$max_leki;$index++)
					{
						$row = $wynik->fetch_assoc();
						echo '<option value='. $row["idleki"] . '>' . $leki[$index] . "</option>";
					}
?>
  					</select>
  				</div>
  						<div class="col-xs-3">
  							<label for="ilosc_pobranie">Ilość</label>
							<input type="number" class="form-control" id="ilosc_pobranie" name="ilosc_pobranie" min="1">
  						</div>
  						<div class="col-xs-2">
  						</br><input type="submit" class="btn btn-info " value="Wykonaj">
  						</div>
  				</div>
				</form>
			</div>
			</div>
		</div>
			
<?php

	if(isset($_POST['nazwa']) && isset($_POST['ilosc']) && isset($_POST['cena']) && isset($_POST['termin']))
	{
		if(!empty($_POST['nazwa']) && !empty($_POST['ilosc']) && !empty($_POST['cena']) && !empty($_POST['termin']))
		{	
			$query="START TRANSACTION";
			$baza->query($query);
			$query="insert into " . $apteczka . " (`idkonta`, `idlek`, `data`, `ile`, `ile_poz`, `dok_zr`, `data_waz`, `cena`, `typ`) values
			" . "('".$_SESSION["zalogowany"]."','".$_POST["nazwa"]."','".date("Y-m-d")."','".$_POST['ilosc']."','".$_POST['ilosc']."',NULL,'".$_POST['termin']."','".$_POST['cena']."','PW')";
			$baza->query($query);
			$query="COMMIT";
			$baza->query($query);
		}
		else
		{
			echo '<div class="alert alert-danger">  <strong>Błąd!</strong> Przynajmniej jeden formularz jest pusty. </div>';
		}
	}
	
	if(isset($_POST['nazwa_pobranie']) && isset($_POST['ilosc_pobranie']))
	{
		if(!empty($_POST['nazwa_pobranie']) && !empty($_POST['ilosc_pobranie']))
		{
			$liczba_lekow_do_pobrania = $_POST['ilosc_pobranie'];
			$query="select * from ".$apteczka;
			$wynik = $baza->query($query);
			$dostepna_liczba_lekow = 0;
			//echo "pob " . $_POST['nazwa_pobranie']."<br>";
			while($row = $wynik->fetch_assoc())
			{
				//echo $row["idlek"];
				if($row["idlek"] == $_POST['nazwa_pobranie'] && time()-(60*60*24) <= strtotime($row["data_waz"]))
					$dostepna_liczba_lekow += $row["ile_poz"];
			}
			if($dostepna_liczba_lekow<$liczba_lekow_do_pobrania)
				echo '<div class="alert alert-danger">  <strong>Błąd!</strong> Niewystarczająca ilość leku w apteczce. </div>';
			else
			{
				$query="START TRANSACTION";
				$baza->query($query);
				while($liczba_lekow_do_pobrania)
				{
					//wybranie paczki o podanym id, o niezerowej liczbie leków i z najwczesniejszą datą ważnosci
					$query="select * from ".$apteczka.", konta, leki_specyfikacja where ".$apteczka.".idkonta=konta.idkonta and ".$apteczka.".idlek=leki_specyfikacja.idleki
					and ".$apteczka.".idlek='".$_POST['nazwa_pobranie']."' and ".$apteczka.".data_waz>=curdate() and ".$apteczka.".ile_poz>0 having min(".$apteczka.".data_waz)";
					$wynik = $baza->query($query);
					$row = $wynik->fetch_assoc();
					$ile_pobrano=0;
					if($row["ile_poz"] < $liczba_lekow_do_pobrania)
					{
						$liczba_lekow_do_pobrania -= $row["ile_poz"];
						$ile_pobrano = $row["ile_poz"];
						$query="update ".$apteczka." set `ile_poz`='0' where id='".$row["id"]."'";
						$baza->query($query);
					}
					else
					{
						$ile_zostalo = $row["ile_poz"] - $liczba_lekow_do_pobrania;
						$ile_pobrano = $liczba_lekow_do_pobrania;
						$liczba_lekow_do_pobrania = 0;
						$query="update ".$apteczka." set `ile_poz`='.$ile_zostalo.' where id='".$row["id"]."'";
						$baza->query($query);
					}
					$query="insert into ".$apteczka." (`idkonta`, `idlek`, `data`, `ile`, `ile_poz`, `dok_zr`, `data_waz`, `cena`, `typ`) values 
					('".$_SESSION["zalogowany"]."','".$_POST['nazwa_pobranie']."','".date("Y-m-d")."','".$ile_pobrano."',NULL,'".$row["id"]."',NULL,NULL,'RW')";
					//echo $query;
					$baza->query($query);
					$query="COMMIT";
					$baza->query($query);
				}
				$query="COMMIT";
				$baza->query($query);
			}
			
		}
		else
		{
			echo '<div class="alert alert-danger">  <strong>Błąd!</strong> Przynajmniej jeden formularz jest pusty. </div>';
		}
	}
	
	$query = "select * from ".$apteczka.", leki_specyfikacja, konta where ".$apteczka.".idkonta=konta.idkonta AND ".$apteczka.".idlek=leki_specyfikacja.idleki";
	$wynik = $baza->query($query);

?>
	<table class="table table-striped">
		<thead>
      		<tr>
        		<th>Nazwa</th>
        		<th>Sub. czynna</th>
        		<th>EAN</th>
        		<th>Op. zbiorcze</th>
        		<th>Ilość</th>
        		<th>Cena</th>
        		<th>Data ważności</th>
        	<th></th>
      		</tr>
    	</thead>
    	<tbody>
<?php

	while($row = $wynik->fetch_assoc()) {
		if($row["ile_poz"] == 0) continue;
		?><tr>
		<td><?php echo $row["nazwa"]; ?></td>
		<td><?php echo $row["subst_czynna"]; ?></td>
		<td><?php echo $row["ean"]; ?></td>
		<td><?php echo $row["op_zb"]; ?></td>
		<td><?php echo $row["ile_poz"]; ?></td>
		<td><?php echo $row["cena"] . " zł"; ?></td>
		<td><?php echo $row["data_waz"];?></td>
		<td>
		</td>
		</tr>
		<?php
	}

?></tbody></table></div><?php
	
	require_once "inc/stopka.php";
?>