<?php 
	session_start();
	require_once 'conf/zmienne.php';
	require_once "inc/$lang/error_msg.php";
	require_once "inc/$lang/teksty.php";
	require_once "inc/funkcje.php";
	require_once 'inc/baza.php';
	
	require_once "inc/nagl.php";
	
	date_default_timezone_set('Europe/Warsaw');
	//echo base64_decode("dG9tYnJ6eQ==");

	if($_GET['wyloguj'] == 1)
		session_destroy();
	
		if(isset($_POST['email']) && isset($_POST['haslo'])){
			$idkonta = sprawdz_login_haslo($_POST['email'], $_POST['haslo']);
			if($idkonta != '')
				$_SESSION['zalogowany'] = $idkonta;
			else $byl_blad_logowania = 2;
		}else if(!isset($_SESSION['zalogowany']))
			session_destroy();
	if( !isset($_GET['wybrano'])){
		header("Location: index.php?wybrano=0");
	}else
		$opcja = ($_GET['wybrano']);
	
	require_once "inc/menu.php";
		
	if(!isset($_SESSION['zalogowany'])){
?>

<div class = "container">
	<h1 class="text-center">Apteczka domowa</h1>
	<h4 class="text-center">Zaloguj się do systemu</h4>
	<br>
<?php 
	if($byl_blad_logowania == 2)
	{
?>
<div class="col-sm-offset-3 col-sm-6">
<div class="alert alert-danger">
  Wprowadzono nieprawidłowy login lub hasło.
</div>
</div>
<br>
<?php }
?>
	<form action="" method="POST">
	
	<div class="row">
    <div class="col-sm-offset-3 col-sm-6">
    
  		<div class="input-group">
    		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    		<input id="email" type="text" class="form-control" name="email" placeholder="Email">
  		</div>
  	<br>
  		<div class="input-group">
    		<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    		<input id="haslo" type="password" class="form-control" name="haslo" placeholder="Hasło">
  		</div>
	<br>
	<div class="row">
		<div class="col-sm-offset-4 col-sm-2"><input type="submit" class="btn btn-info center-block" value="Zaloguj"></div>
		<div class="col-sm-2"><a href="rejestracja.php" class="btn btn-link" role="button">Rejestracja</a></div>
	</div>
	</div>
	</div>
	
	</form>
	
</div>
<?php
	}else{
?>
<div class = "container">
<?php
	$query = "select * from konta where `idkonta` = '" . $_SESSION['zalogowany'] . "'";
	$wynik = $baza->query($query);
	$row = $wynik->fetch_assoc();
?>
<h6>Zalogowano jako: <?php echo $row["imie"] . " " . $row["nazwisko"];?></h6>
<br>
<?php
?><div class="panel panel-default">
	<div class="panel-heading">Leki do zutylizowania</div>
  	<div class="panel-body">
	<ul class="nav nav-tabs">
<?php 
	$apteczka_num = 1;
	$query = "SELECT * FROM apteczki WHERE `idkonta` ='" . $_SESSION['zalogowany'] . "'";
	$wynik = $baza->query($query);
	$apteczka = "";
	if(isset($_GET["apteczka"]))
		$apteczka = $_GET["apteczka"];
	while($row = $wynik->fetch_assoc())
	{
		if(!isset($_GET["apteczka"]) && $apteczka_num == 1)
		{
			$apteczka = $row["nazwa"];
		}
?>	
    	<li <?php if((!isset($_GET["apteczka"]) && $apteczka_num == 1) || $_GET["apteczka"] ==  $row["nazwa"]) echo 'class="active"'; ?>><a href="index.php?wybrano=0&apteczka=<?php echo $row["nazwa"];?>">Apteczka <?php echo $apteczka_num++;?></a></li>
<?php 
	}
	
	$query = "SELECT * FROM dostep WHERE `idkonta` ='" . $_SESSION['zalogowany'] . "'";
	$wynik = $baza->query($query);
	while($row = $wynik->fetch_assoc())
	{
		$query = "SELECT * FROM apteczki, konta WHERE apteczki.idkonta = '" . $row["dostep_do_konta"] . "' AND apteczki.idkonta = konta.idkonta";
		$wynik2 = $baza->query($query);
		while($row2 = $wynik2->fetch_assoc())
		{	
			
			$label = $row2["imie"][0] . $row2["nazwisko"][0];
?>
			<li <?php if($_GET["apteczka"] ==  $row2["nazwa"]) echo 'class="active"';?>><a href="index.php?wybrano=0&apteczka=<?php echo $row2["nazwa"];?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $row2["imie"] . " " . $row2["nazwisko"] ?>">Apteczka <?php echo $apteczka_num++;?> 
			<span class="label label-default"><?php echo $label ?></span> </a></li>
<?php
		}
	}
?>    
  	</ul>
<?php
	$query = "select * from ".$apteczka.", konta, leki_specyfikacja where ".$apteczka.".idkonta=konta.idkonta AND ".$apteczka.".idlek=leki_specyfikacja.idleki AND ".$apteczka.".data_waz<curdate()";
	$wynik = $baza->query($query);
	
	$index=1;
	while($row = $wynik->fetch_assoc())
	{
		if(isset($_POST["recycle_".$index++]))
		{
			$query="START TRANSACTION";
			$baza->query($query);
			$query="update ".$apteczka." set `ile_poz`='0' where `id`='".$row["id"]."'";
			$baza->query($query);
			$query="insert into ".$apteczka." (`idkonta`, `idlek`, `data`, `ile`, `ile_poz`, `dok_zr`, `data_waz`, `cena`, `typ`) values 
			('".$_SESSION["zalogowany"]."','".$row["idlek"]."','".date("Y-m-d")."','".$row["ile"]."',NULL,'".$row["id"]."',NULL,NULL,'UT')";
			$baza->query($query);
			$query="COMMIT";
			$baza->query($query);
		}
	}
	
	$query = "select * from ".$apteczka.", konta, leki_specyfikacja where ".$apteczka.".idkonta=konta.idkonta AND ".$apteczka.".idlek=leki_specyfikacja.idleki AND ".$apteczka.".data_waz<curdate()";
	$wynik = $baza->query($query);
?>
	<table class="table table-striped">
		<table class="table table-striped">
		<thead>
      		<tr>
        		<th>Nazwa</th>
        		<th>Sub. czynna</th>
        		<th>EAN</th>
        		<th>Op. zbiorcze</th>
        		<th>Ilość</th>
        		<th>Cena</th>
        		<th>Data ważności</th>
        		<th></th>
        	<th></th>
      		</tr>
    	</thead>
    	<tbody>
<?php
	$index=1;
	while($row = $wynik->fetch_assoc()) {
		if($row["ile_poz"] == 0) continue;
		?><tr>
		<td><?php echo $row["nazwa"]; ?></td>
		<td><?php echo $row["subst_czynna"]; ?></td>
		<td><?php echo $row["ean"]; ?></td>
		<td><?php echo $row["op_zb"]; ?></td>
		<td><?php echo $row["ile_poz"]; ?></td>
		<td><?php echo $row["cena"] . " zł"; ?></td>
		<td><?php echo $row["data_waz"];?></td>
		<td>
			<form method="post" action="">
				    <button type="submit" class="btn btn-default btn-xs" name=<?php echo '"recycle_'.$index++.'"'?>>
      					<i class="fa fa-recycle" aria-hidden="true"></i>
    				</button>
			</form>
		</td>
		<td>
		</td>
		</tr>
		<?php
	}

?>
</tbody></table>
<?php

?></div></div></div><?php
}//time()-(60*60*24) > strtotime($row['data_waznosci'])
	require_once "inc/stopka.php";
?>