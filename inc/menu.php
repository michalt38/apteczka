<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand">Apteczka domowa</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <?php if($_GET['wybrano'] == 0){?><li class="active"><?php }else{?><li><?php }?><a href="index.php?wybrano=0">Home</a></li>
        <?php if($_GET['wybrano'] == 1){?><li class="active"><?php }else{?><li><?php }?><a href="moje_leki.php?wybrano=1">Moje leki</a></li>
        <?php if($_GET['wybrano'] == 2){?><li class="active"><?php }else{?><li><?php }?><a href="historia.php?wybrano=2">Historia</a></li>
      </ul>
      <?php if(isset($_SESSION['zalogowany'])){?>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="index.php?wyloguj=1"><span class="glyphicon glyphicon-log-out"></span> Wyloguj</a></li>
      <?php }?>
      </ul>
    </div>
  </div>
</nav>